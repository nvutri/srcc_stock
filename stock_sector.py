import json

import requests
import msgpack
import pymongo

from stock_api import MONGODB_URI, API_SECTOR_URL, API_SECTOR_SYMBOL_URL

client = pymongo.MongoClient(MONGODB_URI)
db = client.get_default_database()


class StockSector():
    """Stock Sector data."""

    def __init__(self, sector):
        self.sector = sector
        existing_sector = db['stock_sector'].find_one({'sector': sector})
        if not existing_sector:
            self.get_sector_symbols()
        else:
            self.symbols = existing_sector['symbols']

    def get_sector_symbols(self):
        params = {'sector': self.sector}
        r = requests.post(API_SECTOR_SYMBOL_URL, data=json.dumps(params),
                          headers={'content-type': 'application/json'}, timeout=20)
        self.symbols = msgpack.unpackb(r.content)['data']
        self.store()
        return self.symbols

    @staticmethod
    def get_all_sectors():
        r = requests.post(API_SECTOR_URL, headers={'content-type': 'application/json'}, timeout=20)
        return msgpack.unpackb(r.content)

    def store(self):
        if not db['stock_sector'].find_one({'sector': self.sector}):
            db['stock_sector'].insert({
                'sector': self.sector,
                'symbols': self.symbols
            })
