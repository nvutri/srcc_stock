import argparse
import pymongo
from yahoo_finance import Share
from datetime import datetime
import stock_api

client = pymongo.MongoClient(stock_api.MONGODB_URI)
db = client.get_default_database()

DATE_FORMAT = '%Y-%m-%d'
DEFAULT_START_DATE = datetime.strptime('2001-01-01', DATE_FORMAT)
DEFAULT_END_DATE = datetime.strptime('2007-04-16', DATE_FORMAT)


def get_start_date(symbol_info_start):
    start_date = DEFAULT_START_DATE
    try:
        start_date = datetime.strptime(symbol_info_start, DATE_FORMAT)
        # Save the information to database for symbol information.
        return start_date
    except Exception as e:
        print e
        try:
            splitted_start = symbol_info_start.split('-')
            if splitted_start:
                start_date = datetime.strptime(splitted_start[0], '%Y')
        except Exception as e:
            print e
    return start_date


def get_yahoo_data(db, symbol, end_date):
    """Get symbol data from YAHOO service."""
    sto = Share(symbol)
    print symbol
    symbol_info = sto.get_info()
    print symbol_info
    sto_symbol = db['yahoo_symbols'].find_one({'symbol': symbol})
    if symbol_info.get('start') and symbol_info.get('end'):
        start_date = get_start_date(symbol_info['start'])
        sto_symbol['start'] = start_date
        sto_symbol['end'] = datetime.strptime(symbol_info['end'], DATE_FORMAT)
        db['yahoo_symbols'].save(sto_symbol)
    else:
        start_date = DEFAULT_START_DATE

    historical_data = sto.get_historical(start_date.strftime(DATE_FORMAT), end_date.strftime(DATE_FORMAT))
    for sto_exchange in historical_data:
        try:
            recorded = datetime.strptime(sto_exchange['Date'], DATE_FORMAT)
            if not db['stock_exchange'].find_one({'symbol': sto_exchange['Symbol'], 'recorded': recorded}):
                print sto_exchange
                db['stock_exchange'].save({
                    'symbol': sto_exchange['Symbol'],
                    'volume': sto_exchange['Volume'],
                    'recorded': recorded,
                    'close': sto_exchange['Close'],
                    'open': sto_exchange['Open'],
                    'high': sto_exchange['High'],
                    'low': sto_exchange['Low']
                })
        except KeyError as e:
            print e


def main(args):
    """Get the stock exchange moving averages values."""
    filters = dict()
    if args.sector:
        filters['sector'] = args.sector.replace('_', ' ')
    # Get Yahoo Data.
    for industry in db['yahoo_industries'].find(filters):
        for sto_symbol in db['yahoo_symbols'].find({'industry': industry['industry']}):
            first_exchange = list(
                db['stock_exchange'].find({'symbol': sto_symbol['symbol']}).sort('recorded', pymongo.ASCENDING).limit(
                    1))
            if first_exchange:
                print first_exchange[0].get('recorded')
                successful = False
                while not successful:
                    try:
                        get_yahoo_data(db, sto_symbol['symbol'],
                                       end_date=first_exchange[0]['recorded'])
                    except Exception as e:
                        print e
                    else:
                        successful = True
            else:
                get_yahoo_data(db, sto_symbol['symbol'],
                               end_date=datetime.today())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--sector", help="Industry focusing on the study")
    parsed_args = parser.parse_args()
    main(parsed_args)
