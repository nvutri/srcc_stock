"""Stock symbol class."""
import json
from datetime import datetime

import pymongo
import requests
import msgpack

from stock_exchange import StockExchange
from stock_api import MONGODB_URI, DATA_API_URL


client = pymongo.MongoClient(MONGODB_URI)
db = client.get_default_database()


class Stock():
    """Stock data."""
    DEFAULT_START_DATE = datetime.strptime('1/1/1990', '%m/%d/%Y')
    DEFAULT_END_DATE = datetime.now().today()

    def __init__(self, symbol, start_date=DEFAULT_START_DATE, end_date=DEFAULT_END_DATE):
        self.symbol = symbol
        self.start_date = start_date
        self.end_date = end_date
        self.exchanges = list()
        self.get_exchange_data()

    def get_exchange_data(self):
        if not db['stock_exchange'].find_one({'symbol': self.symbol}):
            params = {'symbol': self.symbol}
            r = requests.post(DATA_API_URL,
                              data=json.dumps(params), headers={'content-type': 'application/json'}, timeout=20)
            data = msgpack.unpackb(r.content)['data']
            self.store_stock(data)
        exchange_filter = {
            'symbol': self.symbol,
            'recorded': {
                '$gte': self.start_date,
                '$lte': self.end_date
            }
        }
        self.exchanges = [StockExchange(rec) for rec in db['stock_exchange'].find(exchange_filter)]

    @staticmethod
    def get_all_symbols():
        return db['stock_symbol'].distinct('symbol')

    def store_stock(self, data):
        exchange_records = []
        for rec in data:
            exchange_records.append(StockExchange.to_dict(rec))
        db['stock_exchange'].insert(exchange_records)
