import argparse
import sys
import os
import requests
from selenium import webdriver
import csv

sys.path.append(os.path.abspath('../'))

import pymongo
import stock_api

client = pymongo.MongoClient(stock_api.MONGODB_URI)
db = client.get_default_database()

NUM_THREADS = 20

INDUSTRY_URL = 'http://biz.yahoo.com/p/sum_conameu.html'
SECTOR_URL = 'http://biz.yahoo.com/p/s_conameu.html'

driver = webdriver.Firefox()


def get_industries(sector):
    """Get industries organized by Yahoo."""
    driver = webdriver.Firefox()
    dataset_industry = db['yahoo_industries']
    driver.get(sector['link'])
    rows = driver.find_elements_by_css_selector('a')
    industries = []
    for row in rows:
        if 'conameu.html' in row.get_attribute('href'):
            rec = {
                'industry': row.text.strip(),
                'sector': sector['sector'],
                'link': row.get_attribute('href')
            }
            print rec
            industries.append(rec['industry'])
            if not dataset_industry.find_one(rec):
                dataset_industry.insert(rec)
    driver.quit()
    return industries


def get_sectors():
    """Get sectors organized by Yahoo."""
    driver = webdriver.Firefox()
    dataset_sectors = db['yahoo_sectors']
    driver.get(SECTOR_URL)
    rows = driver.find_elements_by_css_selector('a')
    for row in rows:
        if 'conameu.html' in row.get_attribute('href'):
            print row.get_attribute('href')
            print row.text
            if not dataset_sectors.find_one({'sector': row.text}):
                dataset_sectors.insert({
                    'sector': row.text,
                    'link': row.get_attribute('href')
                })

    driver.quit()


def get_symbols(industry):
    """Get information related to a symbol from Yahoo."""
    driver.get(industry['link'])
    rows = driver.find_elements_by_tag_name('font')
    symbols = []
    for row in rows:
        if row.text:
            a_elems = row.find_elements_by_tag_name('a')
            if a_elems and len(a_elems) > 1:
                if a_elems[1].get_attribute('href'):
                    if 'finance.yahoo.com' in a_elems[1].get_attribute('href'):
                        rec = {
                            'company': a_elems[0].text,
                            'symbol': a_elems[1].text,
                            'link': a_elems[1].get_attribute('href'),
                            'industry': industry['industry'],
                            'sector': industry['sector']
                        }
                        if not db['yahoo_symbols'].find_one(rec):
                            db['yahoo_symbols'].insert(rec)
                            print rec
                        symbols.append(rec['symbol'])
    return symbols


def main(args):
    """Get the model using classifer and save it."""
    # get_sectors()
    # for sector in db['yahoo_sectors'].find():
    # sector['industries'] = get_industries(sector)
    #     db['yahoo_sectors'].save(sector)
    for industry in db['yahoo_industries'].find({'sector': args.sector.replace('_', ' ')}):
        industry['symbols'] = get_symbols(industry)
        db['yahoo_industries'].save(industry)
    driver.quit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--sector", default={'$ne': None}, help="Industry focusing on the study")
    parsed_args = parser.parse_args()
    main(parsed_args)
