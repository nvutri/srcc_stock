"""URL API to databases."""
LOCAL_MONGODB_URI = 'mongodb://localhost:12252/srcc_stock'
REMOTE_MONGODB_URI = 'mongodb://130.39.189.47:8080/srcc_stock'

MONGODB_URI = REMOTE_MONGODB_URI

DATA_API_URL = 'http://dataquants.org:8080/getDataForSymbol'

API_SECTOR_URL = 'http://dataquants.org:8080/getSector'
API_SECTOR_SYMBOL_URL = 'http://dataquants.org:8080/getSymbolsForSector'
