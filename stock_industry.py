import json

import requests
import msgpack


class StockIndustry():
    """Stock Industry data."""

    API_INDUSTRY_URL = 'http://dataquants.org:8080/getIndustry'
    API_INDUSTRY_SYMBOL_URL = 'http://dataquants.org:8080/getSymbolsForIndustry'

    def __init__(self, industry):
        self.industry = industry

    def _get_industry_symbols(self):
        params = {'industry': self.industry}
        r = requests.post(StockIndustry.API_INDUSTRY_SYMBOL_URL, data=json.dumps(params),
                          headers={'content-type': 'application/json'}, timeout=20)
        return msgpack.unpackb(r.content)

    @staticmethod
    def get_all_industries():
        r = requests.post(StockIndustry.API_INDUSTRY_URL, headers={'content-type': 'application/json'}, timeout=20)
        return msgpack.unpackb(r.content)
