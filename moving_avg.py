import argparse
import pymongo

import stock_api

client = pymongo.MongoClient(stock_api.MONGODB_URI)
db = client.get_default_database()


def moving_avg(symbol, moving_avg):
    """Calculate the moving average for a stock symbol."""
    exchanges = list(db['stock_exchange'].find({'symbol': symbol}).sort('recorded', pymongo.ASCENDING))
    # Get total of the first moving_avg days.
    first_index = 1
    total = sum([exchange['close'] for exchange in exchanges[first_index:moving_avg]])
    moving_avg_label = 'moving_avg_%s' % moving_avg
    for sto_exchange in exchanges[moving_avg:]:
        # Add the today price and calculate the average.
        total = total + sto_exchange['close']
        average = total / moving_avg
        sto_exchange[moving_avg_label] = average
        # Remove the price of the first index entry. Increase the head-index.
        total = total - exchanges[first_index]['close']
        first_index += 1
        db['stock_exchange'].save(sto_exchange)
        print sto_exchange['symbol'], sto_exchange['recorded'], sto_exchange[moving_avg_label], sto_exchange['close']


def main(args):
    """Get the stock exchange moving averages values."""
    filters = dict()
    if args.sector:
        filters['sector'] = args.sector.replace('_', ' ')
    for industry in db['yahoo_industries'].find(filters):
        for symbol in db['yahoo_symbols'].find({'industry': industry['industry']}):
            moving_avg(symbol['symbol'], args.moving_avg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--sector", help="Industry focusing on the study")
    parser.add_argument("--moving_avg", type=int, default=20, help="Moving average")
    parsed_args = parser.parse_args()
    main(parsed_args)
