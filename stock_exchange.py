from datetime import datetime


class StockExchange():
    """Stock data."""

    def __init__(self, rec):
        self.recorded = rec.get('recorded')
        self.symbol = rec.get('symbol')
        self.open = rec.get('open')
        self.high = rec.get('high')
        self.low = rec.get('low')
        self.close = rec.get('close')
        self.volume = rec.get('volume')

    @staticmethod
    def to_dict(exchange_data):
        return {
            'recorded': datetime.strptime(exchange_data[0], '%Y-%m-%d'),
            'symbol': exchange_data[1],
            'open': exchange_data[2] / 100,
            'high': exchange_data[3] / 100,
            'low': exchange_data[4] / 100,
            'close': exchange_data[5] / 100,
            'volume': exchange_data[6]
        }
