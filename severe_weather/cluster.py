import pymongo
import argparse

from sklearn.cluster import KMeans
import numpy as np

REMOTE_MONGODB_URI = 'mongodb://130.39.189.47:8080/srcc_stock'
client = pymongo.MongoClient(REMOTE_MONGODB_URI)
NUM_CLUSTERS = 5


def get_cluster(centers, percentage_label, dataset):
    """Get the cluster given a specified center."""
    cluster_clf = KMeans(init=centers, n_clusters=NUM_CLUSTERS)
    hurricane_training = {}
    for hur in client.srcc_stock['new_hurricanes'].find():
        x_train = []
        symbols = []
        for stat in dataset.find({'hurricane': hur['name']}).sort(percentage_label, 1):
            symbols.append(stat.get('symbol'))
            x_train.append(stat.get(percentage_label, 0))
        hurricane_training[hur['name']] = {'training': x_train, 'symbols': symbols}
    X_train = []
    for hur_training in hurricane_training.values():
        X_train += hur_training['training']
    X_train = np.array(X_train).reshape(-1, 1)
    cluster_clf.fit(X_train)
    return cluster_clf


def main(args):
    """Classify the stock symbols by percentage 20."""
    # Clusterize once. Sort the centers and then cluster again.
    percentage_label = 'percentage_%s' % args.moving
    if args.swing:
        dataset_hurricane_stat = client.srcc_stock['hurricane_improved_stats_%s' % args.swing]
    else:
        dataset_hurricane_stat = client.srcc_stock['hurricane_improved_stats']
    cluster_clf = get_cluster('k-means++', percentage_label, dataset=dataset_hurricane_stat)
    centers = sorted(cluster_clf.cluster_centers_)
    print centers
    cluster_clf = get_cluster(np.array(centers), percentage_label, dataset=dataset_hurricane_stat)

    for hur in client.srcc_stock['new_hurricanes'].find():
        print hur['name']
        x_train = []
        symbols = []
        for stat in dataset_hurricane_stat.find({'hurricane': hur['name']}):
            symbols.append(stat.get('symbol'))
            x_train.append(stat.get(percentage_label, 0))
        if x_train:
            clusters = cluster_clf.predict(np.array(x_train).reshape(-1, 1))
            for x, cluster, symbol in zip(x_train, clusters, symbols):
                stat = dataset_hurricane_stat.find_one({'hurricane': hur['name'], 'symbol': symbol})
                stat['cluster_%s' % args.moving] = int(cluster)
                print stat['symbol'], stat['cluster_%s' % args.moving]
                dataset_hurricane_stat.save(stat)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--swing", type=int, default=None, help="Number of swing days, since hurricane begins.")
    parser.add_argument("--moving", type=int, default=20, help="Number of swing days, since hurricane begins.")
    parsed_args = parser.parse_args()
    main(parsed_args)
