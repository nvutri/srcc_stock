import argparse
import sys
import os

sys.path.append(os.path.abspath('../'))

import pymongo
import csv
import stock_api

WEATHER_EVENTS = ['humberto_09_10_2007_09_28_2007', 'gustav_08_23_2008_09_23_2008', 'ike_08_29_2008_09_29_2008',
                  'irene_08_19_2011_09_19_2011', 'isaac_08_19_2012_09_19_2012', 'alex_06_25_2010_07_25_2010',
                  'dolly_07_18_2008_08_18_2008', 'karl_09_12_2010_10_12_2010', 'matthew_09_20_2010_10_20_2010',
                  'sandy_10_20_2012_11_20_2012']

client = pymongo.MongoClient(stock_api.MONGODB_URI)
db = client.get_default_database()


def get_stocks(percentage, min_close, sector=None):
    """Get set of stocks that satisfy above character."""
    stocks = list()
    stock_filter = {'min_close': {'$gte': min_close},
                    '$or': [{'percent_difference': {'$gte': percentage}},
                            {'percent_difference': {'$lte': -percentage}}]}
    if sector:
        stock_filter['sector'] = sector.replace('_', ' ')
    for ev in WEATHER_EVENTS:
        for rec in db[ev].find(stock_filter):
            stocks.append(rec['symbol'])
    return list(set(stocks))


def consistent_trend(stock_symbol):
    """Check if stock trend is consitent through out the events."""
    FAULT_LIMIT = 0
    positive_trend = 0
    negative_trend = 0
    for ev in WEATHER_EVENTS:
        stock = db[ev].find_one({'symbol': stock_symbol})
        if stock:
            positive_trend += (stock['percent_difference'] > 0)
            negative_trend += (stock['percent_difference'] < 0)
    if positive_trend > FAULT_LIMIT and negative_trend > FAULT_LIMIT:
        print 'False', stock_symbol
        return False
    else:
        print 'TRue', stock_symbol
        return True


def print_result(symbols, csvfile):
    """Print output symbols to file."""
    WRITER_KEYS = ['sector', 'symbol', 'percent_difference', 'min_close', 'max_close', '_id']
    with open(csvfile, 'w') as fp:
        wr = csv.DictWriter(fp, WRITER_KEYS)
        wr.writeheader()
        for symbol in symbols:
            for ev in WEATHER_EVENTS:
                sto = db[ev].find_one({'symbol': symbol})
                if sto:
                    wr.writerow(sto)


def main(args):
    """Get the model using classifer and save it."""
    stock_symbols = get_stocks(args.percentage, args.min_value, args.sector)
    filtered_symbols = []
    for symbol in stock_symbols:
        if consistent_trend(symbol):
            filtered_symbols.append(symbol)
    csv_file = 'analysis_%s_%s.csv' % (args.percentage, args.min_value)
    print_result(filtered_symbols, csv_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("percentage", type=int, help="specify percentage swing to catch")
    parser.add_argument("min_value", type=int, help="specify the min value of the stock")
    parser.add_argument("--sector", help="Sector focusing on the study")
    parsed_args = parser.parse_args()
    main(parsed_args)
