import pymongo
import plotly.plotly as py
from plotly.graph_objs import Scatter, Marker, Figure, Data, Layout, XAxis, YAxis
import argparse

py.sign_in('nvutri', 'm4ql596z2e')
REMOTE_MONGODB_URI = 'mongodb://130.39.189.47:8080/srcc_stock'
client = pymongo.MongoClient(REMOTE_MONGODB_URI)


def main(args):
    if args.swing:
        dataset_hurricane_stat = client.srcc_stock['hurricane_improved_stats_%s' % args.swing]

    else:
        dataset_hurricane_stat = client.srcc_stock['hurricane_improved_stats']
    percentage_label = 'percentage_%s' % args.moving
    traces = []
    cluster_sizes = []
    CLUSTER_LABEL = 'cluster_%s' % args.moving
    all_clusters = dataset_hurricane_stat.distinct(CLUSTER_LABEL)
    for cluster in all_clusters:
        cluster_sizes.append(len(list(dataset_hurricane_stat.find({CLUSTER_LABEL: cluster}))))
    for hur in client.srcc_stock['new_hurricanes'].find():
        x_cluster = []
        y_cluster = []
        hur_sizes = []
        texts = []
        for cluster in all_clusters:
            hur_cluster = list(dataset_hurricane_stat.find({CLUSTER_LABEL: cluster, 'hurricane': hur['name']}))
            hur_sizes.append(max(10, min(30, float(len(hur_cluster) * 100) / cluster_sizes[cluster])))
            x_cluster.append(cluster)
            if hur_cluster:
                y_cluster.append(hur_cluster[0].get(percentage_label, 0))
            else:
                y_cluster.append(None)
            description_text = ['Hurricane %s' % hur['name']]
            for stat in hur_cluster:
                description_text.append('%s: %s' % (stat.get('symbol'), stat.get(percentage_label)))
            texts.append('<br>'.join(description_text))
        traces.append(Scatter(
            x=x_cluster,
            y=y_cluster,
            mode='markers',
            type='scatter',
            marker=Marker(size=hur_sizes),
            text=texts,
            name=hur['name']
        ))

    data = Data(traces)
    layout = Layout(
        title='Hurricane Stock Clustering',
        hovermode='closest',
        xaxis=XAxis(
            title='Cluster',
            showgrid=False,
            zeroline=False
        ),
        yaxis=YAxis(
            title='Moving Average Fluctuation',
            showline=False
        ))
    fig = Figure(data=data, layout=layout)
    py.plot(fig, filename='hurricane-stock-cluster-moving-%s-swing-%s' % (args.moving, args.swing))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--swing", type=int, help="Number of swing days, since hurricane begins.")
    parser.add_argument("--moving", type=int, default=20, help="Number of swing days, since hurricane begins.")

    parsed_args = parser.parse_args()
    main(parsed_args)
