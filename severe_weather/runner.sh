#!/bin/bash

SECTORS="misc
building_cement
finance
biotechnology
capital_goods
consumer_durables
technology
public_util
util
construction
funeral_services
transportation
machinery
retail
industrial_goods
basic_materials
telecom
basic_industries
real_estate
consumer_services
energy
auto_truck
health_care
nursing_homes
consumer_non_durables
notes
scientific"

for sector in $SECTORS
do
  screen -dmS $sector python hurricane_classifier.py --sector $sector --moving 20
done
screen -ls
