"""Classify stock for its clustering."""
import argparse
from datetime import datetime
import itertools
from pprint import pprint
from sklearn.neighbors import KNeighborsClassifier, RadiusNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import preprocessing
from sklearn import cross_validation
from sklearn.feature_extraction import DictVectorizer
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, \
    GradientBoostingClassifier, BaggingClassifier, AdaBoostClassifier
from sklearn.svm import SVC, SVR
from sklearn import grid_search

import pymongo
import logging
import numpy as np

logging.basicConfig(filename='hurricane_classifier.log', level=logging.DEBUG)
REMOTE_MONGODB_URI = 'mongodb://130.39.189.47:8080/srcc_stock'
client = pymongo.MongoClient(REMOTE_MONGODB_URI).get_default_database()


class StockClusterClassifier():
    """Classifier of stock clusters given a hurricane."""

    LOW_TRAIN_DATA_LIMIT = 10
    LOW_TEST_DATA_LIMIT = 5

    def __init__(self, extra_args):
        self.extra_args = extra_args
        self.hurricanes = client['new_hurricanes'].distinct('name')
        if extra_args.swing:
            self.dataset_stats = client['hurricane_improved_stats_%s' % extra_args.swing]
        else:
            self.dataset_stats = client['hurricane_improved_stats']
        self.hurricanes = [client['new_hurricanes'].find_one({'name': hurr})
                           for hurr in self.dataset_stats.distinct('hurricane')]
        self._init_labels()
        if extra_args.sector:
            sector = extra_args.sector.replace('_', ' ').title()
            print 'Sector: ', sector
            self.symbols = [stat['symbol'] for stat in self.dataset_stats.find({'sector': sector})]
            self.symbols = list(set(self.symbols))
        elif extra_args.industry:
            industry = extra_args.industry.replace('_', ' ').title()
            print 'Industry: ', industry
            self.symbols = [stat['symbol'] for stat in self.dataset_stats.find({'industry': industry})]
            self.symbols = list(set(self.symbols))
        else:
            self.symbols = self.dataset_stats.distinct('symbol')
        if self.extra_args.small:
            self.symbols = self.symbols[:self.extra_args.small]
        if self.extra_args.SVC:
            self.algorithm = 'SVC'
        elif self.extra_args.random_forest:
            self.algorithm = 'Random Forest'
        else:
            self.algorithm = 'Decision Tree'

    def _init_labels(self):
        sectors = [sector['sector'] for sector in client['yahoo_sectors'].find()]
        industries = []
        for sector in sectors:
            industries += [industry['industry'] for industry in client['yahoo_industries'].find({'sector': sector})]
        areas = []
        hurricane_names = []
        for hurricane in self.hurricanes:
            areas += hurricane['area']
            hurricane_names.append(hurricane['name'])
        areas = list(set(areas))
        self.le_sector = preprocessing.LabelEncoder()
        self.le_sector.fit(sectors)
        self.le_industry = preprocessing.LabelEncoder()
        self.le_industry.fit(industries)
        self.le_hurricane = preprocessing.LabelEncoder()
        self.le_hurricane.fit(hurricane_names)
        self.le_area = preprocessing.LabelEncoder()
        self.le_area.fit(areas)

        all_winds = client['new_hurricanes'].distinct('wind')
        transformed_winds = preprocessing.StandardScaler(all_winds).fit_transform(all_winds)
        self.nl_wind = {wind: transformed for (wind, transformed) in zip(all_winds, transformed_winds)}

        all_pressures = client['new_hurricanes'].distinct('pressure')
        transformed_pressures = preprocessing.StandardScaler(all_pressures).fit_transform(all_pressures)
        self.nl_pressure = {pressure: transformed for (pressure, transformed) in
                            zip(all_pressures, transformed_pressures)}

        all_costs = client['new_hurricanes'].distinct('cost')
        transformed_costs = preprocessing.StandardScaler(all_costs).fit_transform(all_costs)
        self.nl_cost = {cost: transformed for (cost, transformed) in zip(all_costs, transformed_costs)}

        all_years = [hurricane['start_date'].year for hurricane in client['new_hurricanes'].find()]
        self.le_year = preprocessing.LabelEncoder()
        self.le_year.fit(all_years)

    def _get_cluster(self, hurricane, symbol):
        """Get cluster of a hurricane and symbol."""
        stat = self.dataset_stats.find_one({'hurricane': hurricane, 'symbol': symbol})
        if stat:
            return stat.get('cluster_%s' % self.extra_args.moving)
        else:
            return None

    def _get_datasets(self):
        """Get training & testing datasets."""
        train_features = []
        y_train = []

        for symbol in self.symbols:
            sto = client['yahoo_symbols'].find_one({'symbol': symbol})
            for hurricane in self.hurricanes:
                sto_cluster = self._get_cluster(hurricane['name'], symbol)
                if sto_cluster:
                    feature = {
                        'symbol': symbol,
                        'sector': self.le_sector.transform(sto.get('sector')),
                        'industry': self.le_industry.transform(sto.get('industry')),
                        'category': hurricane.get('category'),
                        'cost': hurricane.get('cost'),
                        'wind': self.nl_wind[hurricane.get('wind')],
                        'pressure': self.nl_pressure[hurricane.get('pressure')],
                        'fatality': hurricane.get('fatality'),
                        'month': hurricane['start_date'].month,
                        'year': self.le_year.transform(hurricane['start_date'].year),
                        'hurricane': self.le_hurricane.transform(hurricane['name']),
                    }
                    for area in hurricane['area']:
                        feature[area] = True
                    train_features.append(feature)
                    y_train.append(sto_cluster)
        v = DictVectorizer(sparse=False)
        v.fit(train_features)
        X_train = v.transform(train_features)
        return X_train, y_train, v

    def classify(self):
        """Start classification."""
        X_data, y_data, v = self._get_datasets()
        test_scores = []
        for test_size in range(10, 100, 5):
            print test_size
            X_train, X_test, y_train, y_test = cross_validation.train_test_split(
                X_data, y_data,
                test_size=float(test_size) / 100,
                random_state=22)
            clf = DecisionTreeClassifier(
                max_depth=15,
                min_samples_leaf=30,
                max_leaf_nodes=160,
                # splitter='random'
            )
            clf.fit(X_train, y_train)
            features = [(name, score) for name, score in zip(v.get_feature_names(), clf.feature_importances_)]
            features = sorted(features, key=lambda x: x[1], reverse=True)[:10]
            pprint(features)
            test_score = clf.score(X_test, y_test)
            print test_score
            test_scores.append({
                'test_size': test_size,
                'test_score': test_score,
                'important_features': features
            })
        return test_scores


def main(args):
    if args.swing:
        dataset_stats = client['hurricane_improved_stats_%s' % args.swing]
    else:
        dataset_stats = client['hurricane_improved_stats']
    log_database = client['hurricane_log']
    log_record = {
        'small': args.small,
        'sector': args.sector,
        'industry': args.industry,
        'moving': args.moving,
        'swing': args.swing,
        'timestamp': datetime.now()
    }
    clf = StockClusterClassifier(extra_args=args)
    log_record['scores'] = clf.classify()
    log_record['algorithm'] = clf.algorithm
    print log_record
    log_database.save(log_record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--random_forest', action='store_true', help='Make the classifier Random Forest')
    parser.add_argument('--SVC', action='store_true', help='Make the classifier SVM')
    parser.add_argument('--Neighbor', action='store_true', help='Make the classifier Neighbor')
    parser.add_argument('--Bayes', action='store_true', help='Make the classifier Neighbor')
    parser.add_argument('--Grid', action='store_true', help='Do a grid search.')
    parser.add_argument('--small', type=int, help='Do a small test.')

    parser.add_argument('--sector', help='Sector')
    parser.add_argument('--industry', help='Industry')
    parser.add_argument("--moving", type=int, default=20, help="Number of swing days, since hurricane begins.")
    parser.add_argument("--swing", type=int, help="Number of swing days, since hurricane begins.")
    parsed_args = parser.parse_args()
    main(parsed_args)
