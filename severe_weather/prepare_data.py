import argparse
import sys
import os
from datetime import timedelta

sys.path.append(os.path.abspath('../'))

import pymongo

import stock_api

from severe_weather import SevereWeather
# Has to be in increasing order for program correctness.
MOVING_AVERAGES = [0, 20, 50, 100, 200]
SWING_DAYS = [10, 15, 20, 30, 60]
PERCENT_LIMIT = 90


def get_symbol_percentage(severe_weather, sto, db, swing=0):
    """Start prepare statistic data."""
    start_date = severe_weather.start_date
    end_date = severe_weather.end_date
    print sto['symbol'], start_date, end_date
    start_exchange = list(db['stock_exchange'].find({'symbol': sto['symbol'], 'recorded': {'$lte': start_date}}).sort(
        'recorded', -1).limit(1))
    end_exchange = list(db['stock_exchange'].find({'symbol': sto['symbol'], 'recorded': {'$gte': end_date}}).sort(
        'recorded', 1).limit(1))
    if not start_exchange or not end_exchange:
        return dict()
    start_exchange = start_exchange[0]
    end_exchange = end_exchange[0]
    result = dict(
        symbol=sto['symbol'],
        sector=sto['sector'],
        industry=sto['industry'],
        company=sto['company'],
        hurricane=severe_weather.name,
        start_date=start_date,
        end_date=end_date,
        swing=swing
    )
    for moving_avg in MOVING_AVERAGES:
        moving_avg_label = 'moving_avg_%s' % moving_avg
        start_close = start_exchange.get(moving_avg_label)
        end_close = end_exchange.get(moving_avg_label)
        if start_close and end_close:
            percentage_diff = float(end_close - start_close) / float(end_close)
            percentage_diff *= 100
            result['start_close_%s' % moving_avg] = start_close
            result['end_close_%s' % moving_avg] = end_close
            result['percentage_%s' % moving_avg] = percentage_diff
    return result


def main(args):
    """Get the model using classifer and save it."""
    client = pymongo.MongoClient(stock_api.MONGODB_URI)
    db = client.get_default_database()
    filters = dict()
    if args.sector:
        filters['sector'] = args.sector.replace('_', ' ')
    if args.hurricane:
        hurricane_list = list(db['new_hurricanes'].find({'name': args.hurricane}))
    else:
        hurricane_list = list(db['new_hurricanes'].find())
    swing = args.swing
    stats_data = db['hurricane_stats_%s' % swing]
    for hurricane in hurricane_list:
        hurricane_end = hurricane['end_date'] + timedelta(days=swing)
        severe_weather = SevereWeather(hurricane['name'], hurricane['start_date'], hurricane_end)
        for industry in db['yahoo_industries'].find(filters):
            for sto in db['yahoo_symbols'].find({'industry': industry['industry']}):
                symbol_stats = get_symbol_percentage(severe_weather, sto, db, swing=swing)
                if symbol_stats:
                    print symbol_stats
                    existing_stats = stats_data.find_one({'symbol': sto['symbol'], 'hurricane': severe_weather.name})
                    if not existing_stats:
                        stats_data.save(symbol_stats)
                    else:
                        existing_stats.update(symbol_stats)
                        stats_data.save(existing_stats)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--hurricane", help="Name of the severe weather event")
    parser.add_argument("--sector", help="Sector focusing on the study")
    parser.add_argument("--swing", type=int, default=10, help="Number of swing days, since hurricane begins.")
    parsed_args = parser.parse_args()
    main(parsed_args)
