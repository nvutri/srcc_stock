"""Classify stock for its clustering."""
import argparse
import itertools
from sklearn.tree import DecisionTreeClassifier
from sklearn import preprocessing
from sklearn.feature_extraction import DictVectorizer
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
import pymongo
import logging
import numpy as np

logging.basicConfig(filename='direct_classifier.log', level=logging.DEBUG)
REMOTE_MONGODB_URI = 'mongodb://130.39.189.47:8080/srcc_stock'
client = pymongo.MongoClient(REMOTE_MONGODB_URI).get_default_database()


class StockClusterClassifier():
    """Classifier of stock clusters given a hurricane."""

    def __init__(self, train_hurricanes, target_hurricane, test_hurricane, extra_args):
        self.train_hurricanes = train_hurricanes
        self.target_hurricane = target_hurricane
        self.test_hurricane = test_hurricane
        self.extra_args = extra_args
        self._init_labels()
        if self.extra_args.SVC:
            self.algorithm = 'SVC'
        elif self.extra_args.random_forest:
            self.algorithm = 'Random Forest'
        else:
            self.algorithm = 'Decision Tree'

    def _init_labels(self):
        sectors = [sector['sector'] for sector in client['yahoo_sectors'].find()]
        industries = [industry['industry'] for industry in client['yahoo_industries'].find()]
        self.le_sector = preprocessing.LabelEncoder()
        self.le_sector.fit(sectors)
        self.le_industry = preprocessing.LabelEncoder()
        self.le_industry.fit(industries)

    def _get_cluster(self, hurricane, symbol):
        stat = client['hurricane_improved_stats'].find_one({'hurricane': hurricane, 'symbol': symbol})
        if stat:
            return stat.get('cluster', -1)
        else:
            return -1

    def _get_training(self):
        feature_list = []
        y_train = []
        y_test = []
        for symbol in client['hurricane_improved_stats'].distinct('symbol'):
            if self._get_cluster(self.test_hurricane, symbol) >= 0 and \
                            self._get_cluster(self.target_hurricane, symbol) >= 0:
                sto = client['yahoo_symbols'].find_one({'symbol': symbol})
                feature = {
                    'sector': self.le_sector.transform(sto.get('sector')),
                    'industry': self.le_industry.transform(sto.get('industry'))
                }
                for hurricane_name in self.train_hurricanes:
                    feature[hurricane_name] = self._get_cluster(hurricane=hurricane_name, symbol=symbol)
                y_train.append(self._get_cluster(self.target_hurricane, symbol))
                y_test.append(self._get_cluster(self.test_hurricane, symbol))
                feature_list.append(feature)
        v = DictVectorizer(sparse=False)
        X_train = v.fit_transform(feature_list)
        return X_train, y_train, y_test

    def classify(self):
        """Start classification."""
        X_train, y_train, y_test = self._get_training()
        if self.extra_args.random_forest:
            print 'Random Forest'
            clf = RandomForestClassifier()
        elif self.extra_args.SVC:
            clf = SVC()
        else:
            print 'Decision Tree'
            clf = DecisionTreeClassifier()
        if len(X_train) > 0:
            clf.fit(X_train, y_train)
            print 'Training Score: %s' % clf.score(X_train, y_train)
            # print 'Target: %s' % y_train
            # print 'Result: %s' % y_test
            test_score = clf.score(X_train, y_test)
            print 'Test Score: %s' % test_score
            logging.info('%s] %s -> %s: %s ' % (self.algorithm, self.target_hurricane, self.test_hurricane, test_score))


def main(args):
    all_hurricanes = client['new_hurricanes'].distinct('name')
    if args.target:
        train_hurricanes = list(all_hurricanes)
        print train_hurricanes
        train_hurricanes.remove(args.target)
        train_hurricanes.remove(args.test)
        clf = StockClusterClassifier(train_hurricanes=train_hurricanes, target_hurricane=args.target,
                                     test_hurricane=args.test, extra_args=args)
        clf.classify()
        return
    for target_hurricane, test_hurricane in itertools.permutations(all_hurricanes, 2):
        train_hurricanes = list(all_hurricanes)
        train_hurricanes.remove(target_hurricane)
        train_hurricanes.remove(test_hurricane)
        print target_hurricane, test_hurricane, train_hurricanes
        clf = StockClusterClassifier(train_hurricanes=train_hurricanes, target_hurricane=target_hurricane,
                                     test_hurricane=test_hurricane, extra_args=args)
        clf.classify()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--target", help="Target train hurricane")
    parser.add_argument("--test", help="Test hurricane")
    parser.add_argument('--random_forest', action='store_true', help='Make the classifier Random Forest')
    parser.add_argument('--SVC', action='store_true', help='Make the classifier SVM')

    parsed_args = parser.parse_args()
    main(parsed_args)
