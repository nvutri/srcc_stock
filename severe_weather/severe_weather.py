import sys
import os

sys.path.append(os.path.abspath('../'))

from stock_sector import StockSector


class SevereWeather():
    """Severe weather study."""

    def __init__(self, name, start_date, end_date, sectors=list()):
        """
        :param name: name of the event
        :param start_date: the day that the event begin to affect.
        :param end_date: the day that the event cease to affect.
        :param sectors: the sectors to do the study on.
        """
        self.name = name
        self.start_date = start_date
        self.end_date = end_date
        self.sectors = [StockSector(sector) for sector in sectors]
