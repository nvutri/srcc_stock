#!/bin/bash

HURRICANES="Dolly Alex Humberto Gustav Irene Ike Karl Isaac Sandy Matthew"

for hurricane in $HURRICANES
do
  screen -dmS $hurricane python prepare_data.py --sector others --hurricane $hurricane
done
screen -ls
