#!/bin/bash

SECTORS="Conglomerates
Basic_Materials
Financial
Consumer_Goods
Industrial_Goods
Healthcare
Technology
Services
Utilities"

for sector in $SECTORS
do
  screen -dmS $sector python moving_avg.py --sector $sector
done
screen -ls
